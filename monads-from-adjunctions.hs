{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveFunctor #-}

-- Monads from Adjunctions

-- Theory, string diagrams, etc...

-- Currying adjunction and the state monad
-- The currying adjunction is the adjunction between the functor a |--> axs
-- and the functor a |--> (s -> a)
newtype L s a = L (a, s) deriving Functor
newtype R s a = R (s -> a) deriving Functor

-- The composition of these, the underlying functor of the state monad is thus
newtype State s a = St (R s (L s a))
-- Which, when expanded is just s -> (a, s), our usual state functor.

-- The unit of the adjunction, the natural transformation a --> RLa = s -> (a, s) is
eta :: a -> R s (L s a)
eta x = R $ \s -> L (x, s)

-- The counit (s -> a, s) = LRa --> a
epsilon :: L s (R s a) -> a
epsilon (L (R f, s)) = f s

-- Hence the multiplication RLRLa --> a
-- mu = Repsilon_L
mu :: R s (L s (R s (L s a))) -> R s (L s a)
mu = fmap epsilon

-- The Writer monad comes from the forgetful-free adjunction for M-sets, where
-- M is the monoid specifying the type of the logs.

-- Monad Transformers
newtype MaybeState s a = MS (s -> Maybe (a, s))

runMaybeState :: MaybeState s a -> s -> Maybe (a, s)
runMaybeState (MS h) = h

liftWithId :: (a -> b) -> (a, c) -> (b, c)
liftWithId f (x, y) = (f x, y)

instance Functor (MaybeState s) where
  fmap f (MS h) = MS (fmap (liftWithId f) . h)

instance Applicative (MaybeState s) where
  pure x = MS $ \s -> Just (x, s)
  MS f <*> MS h = MS $ \s -> fmap (liftWithId . fst) (f s) <*> h s

instance Monad (MaybeState s) where
  ms >>= f = MS $ \s -> case runMaybeState ms s of
                              Nothing -> Nothing
                              Just (a, s') -> runMaybeState (f a) s

-- The monad transformer induced by the state monad:
newtype StateT s m a = StT (s -> m (a, s))

runStateT :: StateT s m a -> s -> m (a, s)
runStateT (StT h) = h

instance Monad m => Functor (StateT s m) where
  fmap f (StT h) = StT $ \s -> fmap (liftWithId f) (h s)

instance Monad m => Applicative (StateT s m) where
  pure x = StT (\s -> return (x, s))
  StT f <*> StT h = StT $ \s -> fmap (liftWithId . fst) (f s) <*> h s

join :: Monad m => m (m b) -> m b
-- (>>=) :: m a -> (a -> m b) -> m b
-- id :: m b -> m b
-- Then with this id, >>= becomes
-- (>>=) :: m (m b) -> (m b -> m b) -> m b
-- Hence (>>= id) :: m (m b) -> m b
join = (>>= id)

instance Monad m => Monad (StateT s m) where
  ms >>= f = joinStateT $ fmap f ms
    where
      joinStateT :: Monad m => StateT s m (StateT s m a) -> StateT s m a
      -- runStateT mma :: s -> m (StateT s m a, s)
      -- runStateT mma s :: m (StateT s m a, s)
      -- uncurry runStateT :: (StateT s m a, s) -> m (a, s)
      -- fmap (uncurry runStateT) $ runStateT mma s :: m (m (a, s))
      -- joinStateT $ fmap (uncurry runStateT) $ runStateT mma s :: m (a, s)
      -- \s -> join $ fmap (uncurry runStateT) $ runStateT mma s :: s -> m (a, s)
      joinStateT mma = StT $ \s -> join $ uncurry runStateT <$> runStateT mma s

-- We can now rewrite MaybeState s a as StateT s Maybe a
