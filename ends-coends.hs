{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}

-- Ends and Coends

class Profunctor f where
  dimap :: (a' -> a) -> (b -> b') -> (f a b -> f a' b')

instance Profunctor (->) where
  dimap f g h = g . h . f

-- We want to be able to compose profunctors
-- Composition in this case means the following. Given two profunctors P and
-- Q, the set P<a,b> is the proposition that a and b are related via P. Similarly
-- for Q. Consequently, the set (PQ)(<a,b>) means that a and b are related via the
-- two profunctors. So basically we are dealing with composition of relations.
-- Now given two relations, P and Q, we have a(PQ)b iff there exists an x such
-- that aPxQb.

-- This is an existential type, meaning that in fact we are looking for an x st.
-- q a x and p x b make sense, i.e. the types are nonempty
data Procompose p q a b where
  -- To construct the composite, we need witness that the aforementioned x exists.
  Procompose :: q a x -> p x b -> Procompose p q a b

-- This is indeed results in a Profunctor
instance (Profunctor q, Profunctor p) => Profunctor (Procompose p q) where
  dimap l r (Procompose qax pxb) = Procompose (dimap l id qax) (dimap id r pxb)

-- This is similar to the mapOut function of the sum type: there we needed a
-- function for each member of the sum, then a finite amount. Here, the situation
-- is the same, but for infinitely many members.
mapOut :: (forall x. q a x -> p x b -> c) -> (Procompose p q a b -> c)
mapOut f (Procompose qax pxb) = f qax pxb

data Coend p where
  Coend :: p x x -> Coend p

-- Ex. 12.2.1.
newtype ProPair q p a b x y = ProPair (q a y, p x b)

instance (Profunctor p, Profunctor q) => Profunctor (ProPair q p a b) where
  dimap l r (ProPair (qay, pxb)) = ProPair (dimap id r qay, dimap l id pxb)

-- Ex. 17.2.2.
newtype CoEndCompose p q a b = CoEndCompose (Coend (ProPair q p a b))

instance (Profunctor p, Profunctor q) => Profunctor (CoEndCompose p q) where
  -- Coend (ProPair q p a b) = ProPair q p a b x x = ProPair (q a x, p x b)
  dimap l r (CoEndCompose (Coend (ProPair (qax, pxb)))) =
    CoEndCompose $ Coend $ ProPair (dimap l id qax, dimap id r pxb)

-- Ends

type End p = forall x. p x x

newtype ProF f g a b = ProF (f a -> g b)

instance (Functor f, Functor g) => Profunctor (ProF f g) where
  dimap l r (ProF h) = ProF $ fmap r . h . fmap l

-- This is in fact category theoretically correct:
-- myHead :: End (ProF [] Maybe)
-- myHead [] = Nothing
-- myHead (x : xs) = Just x

newtype Yo f a x y = Yo ((a -> x) -> f y)

yoneda :: Functor f => End (Yo f a) -> f a
yoneda (Yo alpha) = alpha id

yoneda_inv :: Functor f => f a -> End (Yo f a)
yoneda_inv fa = Yo (\h -> fmap h fa)

data CoY f a x y = CoY (x -> a) (f y)

coyoneda :: Functor f => Coend (CoY f a) -> f a
coyoneda (Coend (CoY g fa)) = fmap g fa

coyoneda_inv :: Functor f => f a -> Coend (CoY f a)
coyoneda_inv fa = Coend (CoY id fa)

-- Ex 17.7.1.
data Day f g x where
  Day :: ((a, b) -> x) -> f a -> g b -> Day f g x

instance (Functor f, Functor g) => Functor (Day f g) where
  fmap f (Day h fa gb) = Day (f . h) fa gb

-- Free Applicatives

data FreeA f x where
  DoneA :: x -> FreeA f x
  MoreA :: ((a, b) -> x) -> f a -> FreeA f b -> FreeA f x

class Monoidal f where
  unit :: f ()
  (>*<) :: f a -> f b -> f (a, b)

instance Functor f => Monoidal (FreeA f) where
  unit = DoneA ()
  (DoneA x) >*< fry = fmap (x,) fry
  (MoreA abx fa frb) >*< fry = MoreA (reassoc abx) fa (frb >*< fry)

reassoc :: ((a, b) -> x) -> (a, (b, y)) -> (x, y)
reassoc abx (a, (b, y)) = (abx (a, b), y)

instance Functor f => Applicative (FreeA f) where
  pure = DoneA
  ff <*> fx = fmap app (ff >*< fx)

app :: (a -> b, a) -> b
app (f, x) = f x

instance Functor f => Functor (FreeA f) where
  fmap f (DoneA x) = DoneA $ f x
  fmap f (MoreA abx fa frb) = MoreA (f . abx) fa frb

-- Monads in the bicategory of profunctors
class Profunctor p => PreArrow p where
  (>>>) :: p a x -> p x b -> p a b
  arr :: (a -> b) -> p a b


-- Lenses

data Lens' s a where
  Lens' :: (s -> (c, a), (c, a) -> s) -> Lens' s a
  
toGet' :: Lens' s a -> (s -> a)
toGet' (Lens' (l, r)) = snd . l

toSet' :: Lens' s a -> (s -> a -> s)
toSet' (Lens' (l, r)) s a = r (fst $ l s, a)

-- Type-changing lens
data Lens s t a b where
  Lens :: (s -> (c, a)) -> ((c, b) -> t) -> Lens s t a b
  
toGet :: Lens s t a b -> (s -> a)
toGet (Lens l r) = snd . l

toSet :: Lens s t a b -> (s -> b -> t)
toSet (Lens l r) s b = r (fst $ l s, b)

prodLens :: Lens (c, a) (c, b) a b
prodLens = Lens id id

compLens :: Lens a b a' b' -> Lens s t a b -> Lens s t a' b'
compLens (Lens l1 r1) (Lens l2 r2) = Lens l3 r3
  where l3 = assoc' . bimap id l1 . l2
        r3 = r2 . bimap id r1 . assoc

-- The associators of the product functor
assoc :: ((c, c'), b') -> (c, (c', b'))
assoc ((c, c'), b') = (c, (c', b'))

assoc' :: (c, (c', a')) -> ((c, c'), a')
assoc' (c, (c', a')) = ((c, c'), a')

class Bifunctor f where
  bimap :: (a -> a') -> (b -> b') -> f a b -> f a' b'

instance Bifunctor (,) where
  bimap f g (a, b) = (f a, g b)