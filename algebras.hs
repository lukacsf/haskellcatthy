{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Prelude hiding (Just, Maybe, Nothing, reverse)

-- Algebras

-- The point of algebras is to provide a universal interface to recursive data
-- types such as trees and lists.

data ExprF x = ValF Int | PlusF x x

instance Functor ExprF where
  fmap f (ValF n) = ValF n
  fmap f (PlusF a b) = PlusF (f a) (f b)

-- At this point we are only dealing with the simplest case. We basically
-- assume that the espression has almost been evaluates, and we are looking at
-- the last remaining step.
evalF :: ExprF Int -> Int
evalF (ValF n) = n
evalF (PlusF n m) = n + m

prettyF :: ExprF String -> String
prettyF (ValF n) = show n
prettyF (PlusF s t) = s ++ " + " ++ t

-- Both evalF and prettyF are examples of evaluators, or structure maps of an
-- algebra.

-- An algebra is an Eilenberg-Moore algebra for the endofunctor f.
-- For an Eilenberg-Moore algebra (c,h), we call c the carrier and h the
-- sturcture map
type Algebra f c = f c -> c

-- Clearly Eilenberg-Moore algebras form a category. (The Eilenberg-Moore
-- category, even though we did not assume that f is part of a monad, but that
-- just means that the forgetful functor might not have a left adjoint.)

-- The evaluator is not a polymorphic function, it is a specific choice for
-- every type c
-- We can rephrase the previous two functions as follows:
evalAlg :: Algebra ExprF Int
evalAlg (ValF n) = n
evalAlg (PlusF m n) = m + n

prettyAlg :: Algebra ExprF String
prettyAlg (ValF n) = show n
prettyAlg (PlusF s t) = s ++ " + " ++ t

-- Exercise 12.2.1
-- show is not an algebra morphism, since pretty . fmap show $ (PlusF 12 54) =
-- "12 + 54", but show . eval $ (PlusF 12 54) = "66"

-- Exercise 12.2.2.
data FloatF x = Num Float | Op x x

addAlg :: Algebra FloatF Float
addAlg (Num x) = x

addAld (Op x y) = x + y

multAlg :: Algebra FloatF Float
multAlg (Num x) = x
multAlg (Op x y) = x * y

-- Let x,y numbers (represented by terms of type Float). For (Num x) the
-- algebra morphism property look as follows:
-- addAlg . fmap log $ (Num x) = addAlg (Num $ log x) = log x
-- log . multAlg $ (Num x) = log x
-- For (Op x y) this would be:
-- addAlg . fmap log (Op x y) = addAlg (Op $ log x log y) = log x + log y = log (x*y)
-- log . multAlg $ (Op x y) = log (x*y)

-- Initial algebra
-- It is interesting to look at the initial object in the Eilenberg-Moore
-- category of f. It can be shown that if (i,iota) is the initial algebra, then
-- Fi=i, making it a fixed point of the functor.
-- Now consider what a fixed point would be like for ExprF
data Expr = Val Int | Plus Expr Expr

-- Clearly Expr = ExprF Expr

-- Let us call Fix f the fixed point of the functor f:
data Fix f where
  -- We think of In as the structure map of the initial algebra, whose carrier
  -- os Fix f
  In :: f (Fix f) -> Fix f

-- Equivalently: In :: Algebra f (Fix f)

-- This is the inverse of In
-- (Remember that Fi=i, so In has an inverse)
out :: Fix f -> f (Fix f)
out (In x) = x

val :: Int -> Fix ExprF
val n = In (ValF n)

plus :: Fix ExprF -> Fix ExprF -> Fix ExprF
plus e1 e2 = In (PlusF e1 e2)

e9 :: Fix ExprF
e9 = plus (plus (val 2) (val 3)) (val 4)

-- Given an algebra alg, we call the corresponfing morphism from the initial
-- algebra to alg cata alg.
cata :: Functor f => forall a. Algebra f a -> Fix f -> a
cata alg = alg . fmap (cata alg) . out

-- What this gets us is one data type (Fix f) and one library function (cata)
-- that encapsulates all the recursive parts. We have only to supple the
-- algebra alg and the functor f.

data Maybe a = Nothing | Just a

-- Algebra structure
algMaybe :: Algebra Maybe Int
algMaybe Nothing = 0
algMaybe (Just x) = x

-- The fixed point of Maybe is the type that has terms Nothing wrapped in
-- arbitrary number of Just. So we can put Nothing=Z and Just=S to get that it
-- is just Nat
-- To construct terms of type Fix Maybe
emptyMaybe :: Maybe a -> Fix Maybe
emptyMaybe Nothing = In Nothing

wrapMaybe :: Fix Maybe -> Fix Maybe
wrapMaybe x = In $ Just x

termFixMaybe :: Fix Maybe
termFixMaybe = wrapMaybe $ wrapMaybe $ wrapMaybe $ emptyMaybe Nothing

-- Now cata algMaybe termFixMaybe evaluates, and evaluates to whatever the
-- velue of algMaybe Nothing is.

-- Lists as initial algebras

-- The list type is the fixed point of the following functor:
data ListF a x = NilF | ConsF a x

instance Functor (ListF a) where
  fmap f NilF = NilF
  fmap f (ConsF a x) = ConsF a $ f x

-- Algebra structure(s)
algLength :: Algebra (ListF a) Int
algLength NilF = 0
algLength (ConsF a x) = 1 + x

algPretty :: Show a => Algebra (ListF a) String
algPretty NilF = ""
algPretty (ConsF a x) = show a ++ ", " ++ x

algToList :: Algebra (ListF a) [a]
algToList NilF = []
algToList (ConsF x xs) = x : xs

-- In
createEmpty :: Fix (ListF a)
createEmpty = In NilF

appendFix :: a -> Fix (ListF a) -> Fix (ListF a)
appendFix a as = In $ ConsF a as

myList :: Fix (ListF Int)
myList = appendFix 3 $ appendFix 2 $ appendFix 1 createEmpty

-- Now cata algLength myList evaluates to 3
-- and cata algPretty myList evaluates to "3, 2, 1, "
-- oh yeah

toFixListF :: [a] -> Fix (ListF a)
toFixListF = foldr appendFix createEmpty

-- reversing a list:
algRev :: Algebra (ListF a) ([a] -> [a])
algRev NilF = id
algRev (ConsF a f) = \as -> f (a : as)

reverse :: Fix (ListF a) -> [a]
reverse list = cata algRev list []

-- What happens here is the following: out extracts the first element, and
-- algRev prepends it to the list it is building (starting with the empty
-- list). Hence the reversing effect.

-- This is basically the same as the normal cata, just the arguments flipped
cata' :: Functor f => Fix f -> forall a. Algebra f a -> a
cata' (In x) alg = alg (fmap (flip cata' alg) x)

-- This is the inverse of cata'
-- alga is a morphism Algebra f a -> a, that is, (f a -> a) -> a. But a
-- polymorphic function for all a. Hence in particular for Fix f.
-- So we get (f (Fix f) -> Fix f) -> Fix f. But we know a function just like
-- that! In!
uncata :: Functor f => (forall a. Algebra f a -> a) -> Fix f
uncata alga = alga In

-- So these establish that Fix f is the same as (forall a. Algebra f a -> a)
newtype Mu f = Mu (forall a. Algebra f a -> a)

cataMu :: Algebra f a -> (Mu f -> a)
cataMu alg (Mu h) = h alg

fromList :: forall a. [a] -> Mu (ListF a)
fromList as = Mu h
  where
    -- Here h basically maps every algebra to as's decomposition according to
    -- that algebra
    -- e.g.: if as = [x,y,z] then
    -- h alg = (Cons x $ alg (ConsF y $ alg (ConsF z $ alg NilF)))
    h :: forall x. Algebra (ListF a) x -> x
    h alg = go as
      where
        go :: [a] -> x
        go [] = alg NilF
        go (n : ns) = alg (ConsF n (go ns))
-- This makes all the more sense in light of cataMu, which basically selects
-- the appropriate algebra

-- Exercise 12.5.1.
algSum :: Algebra (ListF Int) Int
algSum NilF = 0
algSum (ConsF el sum) = el + sum

sum :: Int
sum = cataMu algSum $ fromList [1, 2, 3, 4]
