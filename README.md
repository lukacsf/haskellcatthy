# haskellCatThy

In this repo I collect all the code I have written while reading and working
through Bartosz Milewski's book, The Dao of Functional Programming. Besides
typing up the examples from the book, I tried to write commentary for the more
challenging parts and also solve the exercises.

## Resources
- [The Dao of Functional Programming](https://github.com/BartoszMilewski/Publications/blob/master/TheDaoOfFP/DaoFP.pdf)
- [Learn You a Haskell for Great Good!](http://learnyouahaskell.com/chapters)
