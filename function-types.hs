{-# LANGUAGE GADTs #-}

-- We want to experiment with defininf standard types
import Prelude hiding (Either, Left, Right, Maybe, Just, Nothing)

-- Function Types

-- We want to have objects in \Hask that correspond to functions between types.
-- That is, we want \Hask to be self-enriched. We will call the object of
-- functions a -> b the exponential object b^a.

-- The most important property of functions is that they can be evaluated. Let
-- us rephrase evaluation in terms of category theory. We need a function f ::
-- a -> b and a term x :: a that produce a term y :: b. The function f is then
-- a term of type b^a and we can replace f by a function () -> b^a and x by
-- () -> a. Evaluation then becomes a function (b^a, a) -> b

myEval :: (a -> b, a) -> b
myEval (f, a) = f a

-- This is the elimination rule for the function type.

-- For the introduction rule, consider a function h: c -> b^a. Lifting h and
-- id_a, we get (h, id_a): c*a -> b^a*a. Now we can apply our evaluation
-- function: f:c*a -> b^a*a -> b. For the introduction rule we demand that for
-- every f:c*a -> b an h:c -> b^a should correspond.

funIntroduction :: ((c, a) -> b) -> c -> a -> b
funIntroduction f z x = f (z, x)

-- And its inverse:
funIntroduction_1 :: (c -> (a -> b)) -> (c, a) -> b
funIntroduction_1 h (z, x) = myEval (h z, x) -- That is, h z x

-- This leads us to the very important notion of currying. Currying can be
-- interpreted in a variety of ways in category theory: among others, as an
-- adjunction which we are going to treat later on.

-- The main idea of currying is that it does not matter whether you have a
-- function of multiple variables or a function that produces a function.

myCurry :: ((c, a) -> b) -> c -> a -> b
myCurry f z x = f (z, x)

myUnCurry :: (c -> (a -> b)) -> ((c, a) -> b)
myUnCurry f (z, x) = f z x

-- Notice that this is exactly the same function as the one proving the
-- introduction rule of function types. This is by design. Notice also that
-- Haskell function are by default curried: that is, functions of multiple
-- variables are implemented as functions returning a function (that might return
-- yet another function, in case e.g. of functions of more than 2 variables)

-- It is also worth noting that -> is left associative in Haskell, so a -> (b
-- -> c) is the same as a -> b -> c, but different from (a -> b) -> c!

-- The functoriality of the function type is a bit trickier: in the exponent it
-- is contravariant.
funcLiftFunction :: (a' -> a) -> (b -> b') -> (a -> b) -> (a' -> b')
funcLiftFunction a'toa btob' atob = btob' . atob . a'toa
