{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}

import Data.Kind

-- Dependent Types

-- Dependent types are types parametrised by values. (Rather than, as in the
-- case of functors, other types)

data Nat where
  Z :: Nat
  S :: Nat -> Nat

-- Vec is a dependent type parametrised by natural numbers.
-- Semantically, it encodes the length of the vector
data Vec (n :: Nat) (a :: Type) where
  VNil :: Vec Z a
  VCons :: a -> Vec n a -> Vec (S n) a

emptyV :: Vec Z Int
emptyV = VNil

singleV :: Vec (S Z) Int
singleV = VCons 42 VNil

-- This is guaranteed to only work on Vectors of length at least one.
-- The compiler will complain if we try to call it on VNil
headV :: Vec (S n) a -> a
headV (VCons x _) = x

zipV :: Vec n a -> Vec n a -> Vec n (a, a)
zipV VNil VNil = VNil
zipV (VCons x rest1) (VCons y rest2) = VCons (x, y) $ zipV rest1 rest2

-- Exercise 11.1.1
tailV :: Vec (S n) a -> Vec n a
tailV (VCons x rest) = rest

-- Fibrations
-- The idea of fibration in category theory comes from topology: it is in a
-- sense simply an arrow p: e -> b, but we think of e as the set of fibres
-- p^-1(x) parametrised by elements x of b. Of course these things do not
-- immediately make sense in category theory
-- Example: length: [a] -> Nat. Here the fibres over a natural number n are
-- lists of length n. Clearly lists of type a are parametrised by their length.

-- We can change the base of a fibration p: E -> B via f: A -> B by taking the
-- pullback of the diagram E --> B <-- A:
-- E' ------> E
-- |    g     |
-- |p'        |p
-- |          |
-- ˇ    f     ˇ
-- A -------> B
-- The universal property of the pullback ensures that any other fibration X
-- over A that is also a fibration over B compatible with f and p has a map h:
-- X -> E' Note that this h maps fibers of X to fibers of E' over the same
-- element of A (or B). In this sense it is like fmap: it transforms the
-- contents of the fibers, but does not change their underlying parameters.

-- On the other hand, changing the base of a fibration is more akin to natural
-- transformations.

-- It is possible to extract a single fibre categorically:
-- F ------> E
-- |         |
-- |         |p
-- ˇ         ˇ
-- 1 ------> B

-- A category C is locally cartesian closed, if for every object x of C, the
-- arrow category (C|x) is Cartesian closed. In particular then, (C|x) has
-- products, which are pullbacks in C. If furthermore C has a terminal object,
-- then it is (1) Cartesian closed and (2) has all finite limits

-- Via the pullback, a map f: A -> B specifies a functor f*: (C|B) -> (C|A)
-- We are looking for adjoints (both left and right) to this

-- Dependent sum

-- For an easy example, consider
-- BxF -----> F
--  |         |
--  |p        |
--  ˇ         ˇ
--  B ------> 1
-- This is a pullback.
-- Now let (S, q:S->B, f:S->F) be another cone over the span. Let us fix q.
-- Then the universal property of the pullback says that
-- C(S,F)=(C|B)((S,q),(BxF,p)) (*)
-- Now if we think of S as the sum of its fibres over B, and similarly for BxF
-- (over every point of B there is a copy of F), then a map h:S->BxF in (C|B)
-- is basically a collection of maps from every fiber of S to F. The universal
-- property of the sum would require that there exists a map S->F, that is,
-- from the sum of the domains to the common codomain. But this is exactly what
-- (*) says.

-- Now we can look at it more generally: given a fibration p:F->A and a map f:B->A
-- f*F -----> F
--  |   g     |
--  |f*p      |p
--  ˇ   f     ˇ
--  B ------> A
--  And a cone (S,q:S->B,phi:S->F) over B->A<-F, we have
--  (C|A)((S,fq),(F,p))=(C|B)((S,q),f*(F,p)) (*)
--  We define the depentdent sum functor induced by f as: (S,q) -> (S fq)
--  This is a functor (C|B) -> (B|A)

-- Dependent product

-- These are singleton types
data SNat (n :: Nat) where
  SZ :: SNat Z
  SS :: SNat n -> SNat (S n)

-- Because of the limites support Haskell has for dependent types, we have to
-- pass the second argument as a singleton type
replicateV :: a -> SNat n -> Vec n a
replicateV _ SZ = VNil
replicateV x (SS n) = VCons x (replicateV x n)

-- The following obviously doesn't work:
-- replicateV :: a -> Nat -> Vec ?? a
-- What could ?? be?
-- This is why we need the singleton type

-- replicateV is a dependent product for each term of type a:
-- A dependent product is essentially \Prod_{x:B}T(x). That is, a type T(x) for
-- every term x of type B 'multiplied' together. Observe that replicateV is
-- exactly that: given a term t of type a, it produces a function for each
-- natural number, which equivalent to the tuple
-- ((),(t),(t,t),(t,t,t),(t,t,t,t),...)

-- More generally, given a fibration p: E -> B, the dependent type S(E) is the
-- "set" of global sections of the "bundle". That is, given an element s in
-- S(E) and an element x in B, we can evaluate s at x to get an element in E.
-- It also has to land in the correct fibre: p(s(x))=x. Categorically:
-- S(E)xB ---------> E
--    \      ev     /
--     \           /
--      \         /
--       \p1     /p
--        \     /
--         \   /
--         ˇ  ˇ
--           B
-- So essentially ev is a morphism in (C|B)
-- This looks pretty much like the regular evaluate function for function types.
-- In fact, the dependent product is defined by exactly the same universal
-- property as the function type, but this time in (C|B)
