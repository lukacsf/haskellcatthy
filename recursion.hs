{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

-- Recursion

-- To illustrate recursion, we will take a look the most famous example of
-- recursion in mathematics, the natural numbers. The idea is that every
-- natural number can be constructed from 2 ingredients: zero, successor.
-- The idea is that every natural number except zero is the successor of
-- another natural number.

-- Let Nat be the natural numbers object, Z: () -> Nat the zero 'element' and
-- S: Nat -> Nat the successor function. Then one is S . Z, two is S . S . Z
-- and so on.

-- In Haskell:
data Nat where
  Z :: Nat
  S :: Nat -> Nat

-- This is the introduction rule

-- For the elimination rule, let h: Nat -> a be a function. This immediately
-- gives us a term of type a, namely h . Z: () -> Nat -> a. We call this init.
-- Similarly we have h . S . Z, h . S .S . Z and so on. But this is an infinite
-- amount of information, that is not very practical. So what we are going to
-- demand is the existence of a function step: a -> a such that h . S = step . h.

-- Mappings h: Nat -> a that satisfy the above criterion (i.e. the existence of
-- init and step) are called recursive. Note that not all mappings out of Nat
-- are recursive.

-- In Haskell, recursion can be implemented the following way:
rec :: a -> (a -> a) -> (Nat -> a)
rec init step = \case
  Z   -> init
  S m -> step $ rec init step m

-- This one function called recursor is enough to implement all recursive
-- functions on the natural numbers. For example addition:

plus :: Nat -> Nat -> Nat
plus a = rec a S

-- This function starts at a :: Nat and takes its successor as many times as
-- the second number we provide.

-- An easier to understand implementation:
plus' :: Nat -> Nat -> Nat
plus' n Z = n
plus' n (S m) = S (plus n m)

-- Lists

-- A list is either empty, or its first element followed by the rest of the list

data List a where
  Nil :: List a
  Cons :: (a, List a) -> List a

-- When trying to figure out the elimination rule for lists, we only look at
-- recursive functions h: List a -> c. It has to satisfy h . Cons = step .
-- (id_a &&& h). Again, we want to find out step. (We already know that init =
-- h . Nil). Once again, this is difficult, since h is recursive if and only if
-- it was constructed using some init and step. So we know that h is recursive
-- if and only if we know init and step.

-- But we can still look at how we might construct h from init and step. This
-- is called a fold in Haskell, or, more elegantly, a list catamorphism.

recList :: c -> ((a, c) -> c) -> (List a -> c)
recList init step = \case
  Nil          -> init
  Cons (a, as) -> step (a, recList init step as)

-- Haskell has built-in, much nicer syntax for lists:
-- List a ~ [a]
-- Cons (a, as) ~ (a:as)
-- reclist = foldr

-- Exercise 7.2.3
thirdElement :: [a] -> Maybe a
thirdElement (a:b:c:rest) = Just c
thirdElement _ = Nothing

-- Functoriality of lists:
myMap :: (a -> b) -> [a] -> [b]
myMap f [] = []
myMap f (x:xs) = f x : map f xs
-- myMap f is essentially the image of f under the functor a -> [a]. More on
-- this in the chapter on functors.
