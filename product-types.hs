{-# LANGUAGE GADTs #-}

-- We want to experiment with defininf standard types
import Prelude hiding (Just, Maybe, Nothing)

-- Product Types

-- Recall that a product of two objects is a limit cone a <- a*b -> b with the
-- appropriate universal property
-- In Haskell, we denote the product of two types a and b as (a,b).

prodElimination :: (c -> (a, b)) -> (c -> a, c -> b)
prodElimination f = (fst . f, snd . f)

prodIntroduction :: (c -> a, c -> b) -> (c -> (a, b))
prodIntroduction (f, g) x = (f x, g x)

-- And its curried version:
(&&&) :: (c -> a) -> (c -> b) -> c -> (a, b)
(f &&& g) x = (f x, g x)

-- Notice that terms of type (a,b) are denoted (x,y) where x has type a and y
-- has type b. And indeed, terms of type (a,b) correspond to pairs of terms of
-- type a and b. Remember that terms of type a are functions () -> a. We have
-- discussed above that functions () -> (a,b) are pairs of functions () -> a,
-- () -> b, which in turn correspond to terms of type a and b repsectively.

-- Product is also commutative and associative:
prodComm :: (a, b) -> (b, a)
prodComm (x, y) = (y, x)

prodAss :: ((a, b), c) -> (a, (b, c))
prodAss ((x, y), z) = (x, (y, z))

-- The functoriality of the product means that given functions a -> a' and b ->
-- b', we automatically get a function (a, b) -> (a', b')

prodLiftFunction :: (a -> a', b -> b') -> (a, b) -> (a', b')
prodLiftFunction (f, g) (x, y) = (f x, g y)

-- and its curried version:
prodLiftFunction' :: (a -> a') -> (b -> b') -> (a, b) -> (a', b')
prodLiftFunction' f g = (f . fst) &&& (g . snd)

-- Distributivity of sum and product formation
distributivity :: Either (a, b) (a, c) -> (a, Either b c)
distributivity (Left (x, y)) = (x, Left y)
distributivity (Right (x, z)) = (x, Right z)

-- And its inverse
distributivity_1 :: (a, Either b c) -> Either (a, b) (a, c)
distributivity_1 (x, Left y) = Left (x, y)
distributivity_1 (x, Right z) = Right (x,z)

-- doing it in a point-free way is a bit more tricky:
-- Mapping out of a sum requires two functions:
-- (a,b) -> (a, Either b c) 
-- (a,c) -> (a, Either b c)
-- and then combining them with either, the introduction rule of sum
distPF :: Either (a, b) (a, c) -> (a, Either b c)
distPF = either f g
  where
    f :: (a, b) -> (a, Either b c)
    -- Mapping into a product requires two functions:
    -- (a, b) -> a
    -- (a, b) -> Either b c
    -- and then combining them with &&&
    f = f' &&& f''
    f' :: (a, b) -> a
    f' = fst
    f'' :: (a, b) -> Either b c
    f'' = Left . snd
    g :: (a, c) -> (a, Either b c)
    -- Similarly to f:
    g = g' &&& g''
    g' = fst
    g'' = Right . snd

distPF' :: Either (a, b) (a, c) -> (a, Either b c)
distPF' = either (fst &&& (Left . snd)) (fst &&& (Right . snd))

-- Similarly for its inverse:
undistPF :: (Either b c, a) -> Either (b, a) (c, a)
-- Here we have to use currying: 
-- curry undistPF :: Either b c -> a -> Either (b, a) (c, a)
undistPF = uncurry f
  where
    f :: Either b c -> a -> Either (b, a) (c, a)
    -- mapping out of a sum is two functions:
    -- b -> a -> Either (b, a) (c, a)
    -- c -> a -> Either (b, a) (c, a)
    -- then we uncurry them:
    -- (b, a) -> Either (b, a) (c, a)
    -- (c, a) -> Either (b, a) (c, a)
    -- Clearly these are just Left and Right
    -- And finally we piece them together with either
    f = either g h
    g = curry Left
    h = curry Right

-- Putting it all together
undistPF' :: (Either b c, a) -> Either (b, a) (c, a)
undistPF' = uncurry (either (curry Left) (curry Right))

-- Monoids

-- These are supposed to be the same as monoids in mathematics with mappend
-- being the binary operation and mempty picking out the unit. We expect
-- Haskell monoids to satisfy the monoid axioms: associativity, and unit.
class Monoid m where
  mappend :: (m, m) -> m
  mempty :: () -> m
