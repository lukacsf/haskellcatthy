{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE KindSignatures #-}

import Prelude hiding (Functor, (.), fmap)
import Data.Kind

-- Functors

-- The types and functions of Haskell form a category:
(.) :: (b -> c) -> (a -> b) -> (a -> c)
g . f = \x -> g (f x)

id :: a -> a
id x = x

-- It is easy to see that composition is associative and the id functions are
-- local units.

-- In Haskell we are dealing with endofunctors, that is, functors from the
-- category of types and functions to itself.

-- A type constructor is sometheing that takes a type and produces another. A
-- type constructor f is a functor, if it can also act on functions via a
-- functions called fmap. We also require that it satisfies the functor properties.

class Functor f where
  fmap :: (a -> b) -> (f a -> f b)

-- "Functor" is a typeclass, that is, a special property that typeconstructors
-- can have. If we want to declare that a certain typeconstructor has this
-- property, that is, it is a functor, we have to implement fmap:

instance Functor Maybe where
  fmap g Nothing = Nothing
  fmap g (Just x) = Just (g x)

-- Notice that [] is also a functor, and map is its implementation of fmap.

class Bifunctor f where
  bimap :: (a -> a') -> (b -> b') -> (f a b -> f a' b')

instance Bifunctor (,) where
  bimap f g (x, y) = (f x, g y)

-- Notice that this is the same as prodLiftFunction
-- And similarly for sumLiftFunction

instance Bifunctor Either where
  bimap f _ (Left x) = Left (f x)
  bimap _ g (Right y) = Right (g y)

-- The same would not work for funLiftFunction: we needed a' -> a instead of a
-- -> a' there. The functor (->) is contravariant in the first variable. Such
-- functors are called Profunctors.

class Profunctor f where
  dimap :: (a' -> a) -> (b -> b') -> (f a b -> f a' b')

instance Profunctor (->) where
  dimap f g h = g . h . f

class Contravariant f where
  contramap :: (b -> a) -> (f a -> f b)

-- Here is an example of a contravariant functor
newtype Predicate a = Predicate (a -> Bool)

instance Contravariant Predicate where
  contramap f (Predicate h) = Predicate (h . f)

-- Composing Functors
data Compose :: (Type -> Type) -> (Type -> Type) -> (Type -> Type)
  -- What happens here is that in order to to make a Compose [] Maybe Int out
  -- of a [Maybe Int], we need a type constructor.
  where Compose :: g (f a) -> Compose g f a

-- For example, the way you define something of type Compose [] Maybe Int is
-- the following
x :: Compose [] Maybe Int
x = Compose [Just 12, Nothing, Just 3]

instance (Functor g, Functor f) => Functor (Compose f g) where
  fmap h (Compose gfa) = Compose $ fmap (fmap h) gfa

-- Exercise 8.5.1. Composing a functor and a contravariant
-- We can reuse Compose
instance (Contravariant f, Functor g) => Contravariant (Compose g f) where
  contramap h (Compose gfa) = Compose $ fmap (contramap h) gfa
