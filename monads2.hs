{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE MonoLocalBinds #-}

import Prelude hiding (Applicative, (<*>), pure)

-- Lax monoidal functors are functors between monoidal categories preserving monoids
-- For this we require the existence of morphisms
-- j -> Fi
-- Fa + Fb -> F(a+b) for all a and b

class Monoidal f where
  unit :: f ()
  -- The monoidal structure on Hask is induced by the product
  (>*<) :: f a -> f b -> f (a, b)

-- Exercise 14.8.1
instance Monoidal [] where
  unit = [()]
  xs >*< ys = zip xs ys

-- Functorial strength
-- The strength of an endofunctor is a natural transformation a + Fb -> F(a+b)
pairWith :: Int -> [String -> (Int, String)]
pairWith n = [(n,)]
-- The fact that we could implement this generally means that in Haskell all
-- functors are strong

strength :: Functor f => (e, f a) -> f (e, a)
strength (e, as) = fmap (e,) as

-- In Haskell all monads are monoidal
instance Monad m => Monoidal m where
  unit = return ()
  ma >*< mb = do
    a <- ma
    b <- mb
    return (a, b)

-- Applicative functors
-- Given a functor f we knos how to lift functions of one variable: use fmap
-- What about funcions of two variables? We'd need something like
lift2V :: (a -> b -> c) -> f a -> f b -> f c
lift2V = undefined

-- We can view this as a function that returns a function, so we can apply fmap.
-- But that gives us a term of type f (b -> c). It is not clear how to apply
-- this to a term of type f b and get f c.
-- The cases where this is doable are called applicative.

class Functor f => Applicative f where
  -- This is for zero-variable functions (values)
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b

-- Now we can implement the 2 variable lifting function:
lift2V' :: Applicative f => (a -> b -> c) -> f a -> f b -> f c
lift2V' g fa fb = fmap g fa <*> fb

-- Exercise 14.8.2.
lift3V :: Applicative f => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
lift3V g fa fb fc = fmap g fa <*> fb <*> fc

apply :: (a -> b, a) -> b
apply (f, a) = f a

-- The upshot is that because the Haskell category is cartesian closed, all
-- monoidal functors are applicative
instance (Functor f, Monoidal f) => Applicative f where
  pure a = fmap (const a) unit
  fs <*> as = fmap apply (fs >*< as)

instance Applicative f => Monoidal f where
  unit = pure ()
  xs >*< ys = (,) <$> as <*> bs

-- This also shows that all Monads are Applicatives
ap :: Monad m => m (a -> b) -> m a -> m b
ap fs xs = do
  f <- fs
  x <- xs
  return $ f x
