{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}

import Data.List

-- Coalgebras

-- Coalgebras are dual to algebras: let f be an endofunctor
type Coalgebra f a = a -> f a
type Algebra f a = f a -> a

data TreeF x = LeafF | NodeF Int x x
  deriving (Show)

instance Functor TreeF where
  fmap f LeafF = LeafF
  fmap f (NodeF n left right) = NodeF n (f left) (f right)

coalgSplit :: Coalgebra TreeF [Int]
coalgSplit [] = LeafF
coalgSplit (n : ns) = NodeF n left right
  where
    (left, right) = partition (<= n) ns

-- Similarly to algebras, we can look at the category of F-coalgebras
-- and look at the terminal object. By the dual of Lambek's lemma if (t,tau)
-- is a terminal coalgebra, then Ft=t and tau is an iso

data Fix f where
  In :: f (Fix f) -> Fix f

out :: Fix f -> f (Fix f)
out (In x) = x

ana :: Functor f => Coalgebra f a -> a -> Fix f
ana coalg = In . fmap (ana coalg) . coalg

algToList :: Algebra TreeF [Int]
algToList LeafF = []
algToList (NodeF n left right) = left ++ [n] ++ right

cata :: Functor f => Algebra f a -> Fix f -> a
cata alg = alg . fmap (cata alg) . out

sortList :: [Int] -> [Int]
sortList = cata algToList . ana coalgSplit

-- Infinite data structures
-- The idea is that we don't have to build up data the terms by hand, but can
-- rely on anamorphisms. Thus, we can srap "leaves".

data StreamF a x = StreamF a x

instance Functor (StreamF a) where
  fmap f (StreamF a x) = StreamF a $ f x

-- The fixed point of this functor is an infinite stream
type Stream a = Fix (StreamF a)

coalgStep :: Coalgebra (StreamF Int) Int
coalgStep n = StreamF n (n+1)

-- This allows us to construct a stream of all natural numbers:
allNats :: Stream Int
allNats = ana coalgStep 0

headStr :: Stream a -> a
headStr (In (StreamF a _)) = a

tailStr :: Stream a -> Stream a
tailStr (In (StreamF _ s)) = s

-- Hylomorphism
-- (Not the Aristotelian sort)

-- This is basically the composition of an ana and a cata
hylo :: Functor f => Algebra f b -> Coalgebra f a -> a -> b
hylo alg coalg = alg . fmap (hylo alg coalg) . coalg

-- This makes
qsort :: [Int] -> [Int]
qsort = hylo algToList coalgSplit

-- The definition of an anamorphism can be interpreted as the defining
-- universal property of the terminal coalgebra
-- To see this, we uncurry ana:
ana' :: Functor f => forall a. (a -> f a, a) -> Fix f
ana' (coalg, x) = In (fmap (curry ana' coalg) (coalg x))

data Nu f where
  Nu :: forall a f. (a -> f a, a) -> Nu f

-- It does typecheck...
anaNu :: Functor f => Coalgebra f a -> a -> Nu f
anaNu coalg a = Nu (coalg, a)

headNuStr :: Nu (StreamF a) -> a
headNuStr (Nu (unf, s)) = let (StreamF a _) = unf s in a

tailNuStr :: Nu (StreamF a) -> Nu (StreamF a)
tailNuStr (Nu (unf, s)) = let (StreamF _ t) = unf s in Nu (unf, t)
