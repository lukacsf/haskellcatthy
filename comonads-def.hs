{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TupleSections #-}

-- Comonads

-- Recap: computation with an environment was modelled using the Reader monad
newtype Reader e a = Reader (e -> a)

runReader :: Reader e a -> e -> a
runReader (Reader h) = h

instance Functor (Reader e) where
  fmap f (Reader h) = Reader $ f . h

instance Applicative (Reader e) where
  pure x = Reader $ const x

  -- f :: e -> a -> b
  -- h :: e -> a
  -- out :: a -> b
  Reader f <*> Reader h = Reader $ \e -> f e $ h e

instance Monad (Reader e) where
  -- f :: a -> Reader e a
  -- f :: a -> (e -> a)
  -- h :: e -> a
  Reader h >>= f = Reader $ \e -> runReader (f $ h e) e

-- We can look at computation with a context (environment) as co-Kleisli arrows:
composedWithEnv :: ((b, e) -> c) -> ((a, e) -> b) -> ((a, e) -> c)
composedWithEnv f g (a, e) = f (g (a, e), e)

idWithEnv :: (a, e) -> a
idWithEnv (a, e) = a

-- This leads us to the definition of comonads
class Functor w => Comonad w where
  extract :: w a -> a

  -- This is analogous to the fish operator (<=<) which is the Kleisli composition
  (=<=) :: (w b -> c) -> (w a -> b) -> (w a -> c)

newtype Reader' e a = Reader' (a, e) deriving (Functor)

instance Comonad (Reader' e) where
  extract (Reader' (x, e)) = x
  f =<= g = \(Reader' (a, e)) -> f $ Reader' (g (Reader' (a, e)), e)

instance Comonad ((,) e) where
  extract = snd
  g =<= f = \ea -> g (fst ea, f ea)

-- duplicate can be implemented in terms of extend
extend :: Comonad w => (w a -> b) -> w a -> w b
extend f wa = undefined

duplicate :: Comonad w => w a -> w (w a)
duplicate = extend id

-- and extend can be implemented in terms of duplicate
duplicate'' :: Comonad w => w a -> w (w a)
duplicate'' = undefined

extend'' :: Comonad w => (w a -> b) -> w a -> w b
extend'' f wa = f <$> duplicate'' wa

-- So we might as well define the Comonad class as
class Functor w => Comonad' w where
  extract' :: w a -> a
  duplicate' :: w a -> w (w a)
  extend' :: (w a -> b) -> w a -> w b
  extend' f wa = f <$> duplicate' wa
  (==<==) :: (w b -> c) -> (w a -> b) -> (w a -> c)
  g ==<== f = g . fmap f . duplicate'

